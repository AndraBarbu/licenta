package com.example.rezervarecfrtfc;

import android.support.test.rule.ActivityTestRule;
import android.view.View;

import com.sourcey.materiallogindemo.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

public class RailwayOperatorActivityTest {

    @Rule
    public ActivityTestRule<RailwayOperatorActivity> mActivityTestRule =
            new ActivityTestRule<RailwayOperatorActivity>(RailwayOperatorActivity.class
    );
    private  RailwayOperatorActivity railwayOperatorActivity=null;
    @Before
    public void setUp() throws Exception {
        railwayOperatorActivity =mActivityTestRule.getActivity();
    }

    @Test
    public void testLaunch(){
        View view1 = railwayOperatorActivity.findViewById(R.id.btnCfr);
        assertNotNull(view1);
        View view2 = railwayOperatorActivity.findViewById(R.id.btnTfc);
        assertNotNull(view2);

    }
}