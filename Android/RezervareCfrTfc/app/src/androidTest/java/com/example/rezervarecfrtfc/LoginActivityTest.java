package com.example.rezervarecfrtfc;

import android.support.test.rule.ActivityTestRule;
import android.view.View;

import com.sourcey.materiallogindemo.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

public class LoginActivityTest {

    @Rule
    public ActivityTestRule<LoginActivity> mActivityTestRule = new ActivityTestRule<LoginActivity>(LoginActivity.class
    );
    private  LoginActivity loginActivity=null;
    @Before
    public void setUp() throws Exception {
        loginActivity =mActivityTestRule.getActivity();
    }

    @Test
    public void testLaunch(){
        View view1 = loginActivity.findViewById(R.id.input_email);
        assertNotNull(view1);
        View view2 = loginActivity.findViewById(R.id.input_password);
        assertNotNull(view2);

    }
}