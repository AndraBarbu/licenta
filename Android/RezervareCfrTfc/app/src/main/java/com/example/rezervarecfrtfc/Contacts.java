package com.example.rezervarecfrtfc;

public class Contacts {

    String nume_operator, numar_telefon, email, adresa, fax, operator_url;

    public String  getNumeOperator()
    {
        return nume_operator;
    }

    public void setNumeOperator(String temp)
    {
        this.nume_operator = temp;
    }

    public String  getNumarTelefon()
    {
        return numar_telefon;
    }

    public void setNumarTelefon(String temp)
    {
        this.numar_telefon = temp;
    }

    public String  getEmail()
    {
        return email;
    }

    public void setEmail(String temp)
    {
        this.email = temp;
    }

    public String  getAdresa()
    {
        return adresa;
    }

    public void setAdresa(String temp)
    {
        this.adresa = temp;
    }

    public String  getFax()
    {
        return fax;
    }

    public void setFax(String temp)
    {
        this.fax = temp;
    }

    public String  getOperatorUrl()
    {
        return operator_url;
    }

    public void setOperatorUrl(String temp)
    {
        this.operator_url = temp;
    }


}