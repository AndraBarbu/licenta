package com.example.rezervarecfrtfc.model;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.rezervarecfrtfc.R;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import android.os.AsyncTask;

public class AutoCompleteAdapter extends ArrayAdapter implements Filterable {

    private static final String TAG = AutoCompleteAdapter.class.getName();

    private TextView stationName;
    private Station station;
    private ArrayList mStation;
    private String STATION_URL = "http://192.168.1.6/trenuri_cfr_tfc/search_stations.php?nume_statie=";

    public AutoCompleteAdapter(Context context, int resource) {
        super(context, resource);
        mStation = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mStation.size();
    }

    @Override
    public Station getItem(int position) {
        return (Station) mStation.get(position);
    }

    @Override
    public Filter getFilter() {

        Filter myFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if(constraint != null){
                    try{
                        //get data from the web
                        String term = constraint.toString();
                        mStation = new SugestionStation().execute(term).get();
                    }catch (Exception e) {

                        Log.d(TAG, "Exception " + e);
                    }

                    filterResults.values = mStation;
                    filterResults.count = mStation.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if(results != null && results.count > 0){
                    notifyDataSetChanged();
                }else{
                    notifyDataSetInvalidated();
                }
            }
        };

        return myFilter;
    }

    @Override
    @SuppressLint("ViewHolder")
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.auto_complete_layout, parent,false);

        /** get station. */
        station = (Station) mStation.get(position);

        stationName = view.findViewById(R.id.stationName);

        stationName.setText(station.getName());

        return view;
    }

    /** Fetch station list. */
    @SuppressLint("StaticFieldLeak")
    private class SugestionStation extends AsyncTask<String, Integer, ArrayList<Station>> {

        @Override
        protected ArrayList<Station> doInBackground(String... params) {
            try {

                //Create a new Station SEARCH url Ex "search.php?term=india"
                String NEW_URL = STATION_URL + URLEncoder.encode(params[0],"UTF-8");
                Log.d(TAG, "JSON RESPONSE URL " + NEW_URL);

                URL url = new URL(NEW_URL);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");

                InputStream in = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                StringBuilder sb = new StringBuilder();
                while ((line = reader.readLine()) != null){
                    sb.append(line).append("\n");
                }

                //parse JSON and store it in the list
                String jsonString =  sb.toString();
                ArrayList StationList = new ArrayList<>();

                JSONArray jsonArray = new JSONArray(jsonString);
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    //store the Station name
                    Station Station = new Station();
                    Station.setName(jsonObject.getString("nume_statie"));
                    StationList.add(Station);
                }

                //return the StationList
                return StationList;

            } catch (Exception e) {
                Log.d(TAG, "Exception " + e);
                return null;
            }
        }
    }
}
