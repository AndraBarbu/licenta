package com.example.rezervarecfrtfc;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ProgressBar;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TrainsItineraries extends AppCompatActivity {

    public static final String TAG = TrainsItineraries.class.getName();

    List<Trains> trainList;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager recyclerViewlayoutManager;
    RecyclerView.Adapter recyclerViewadapter;

    String st_plecare, st_sosire, dt_plecare;

    String tip_cursa = "tip_cursa";
    String cod_cursa = "cod_cursa";
    String statie_plecare = "statie_plecare";
    String statie_sosire = "statie_sosire";
    String ora_plecare = "ora_plecare";
    String ora_sosire = "ora_sosire";

    JsonArrayRequest jsonArrayRequest;

    RequestQueue requestQueue;

    /** list of arrays with all the information about trips. */
    ArrayList<String> codCursa, tipCursa, statiePlecare, statieSosire, oraPlecare, oraSosire, dataPlecare;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trains_results);
        Bundle extras = getIntent().getExtras();
        assert extras != null;

        // get extras from intent
        st_plecare = extras.getString("statie_plecare");
        st_sosire = extras.getString("statie_sosire");
        dt_plecare = extras.getString("data_plecare");

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(TrainsItineraries.this);
        @SuppressLint("CommitPrefEdits")
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("data_plecare", dt_plecare);

        /** create an array list of trip trains. */
        trainList = new ArrayList<>();

        recyclerView =  findViewById(R.id.recyclerViewTrainResults);
        recyclerView.setHasFixedSize(false);
        recyclerViewlayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(recyclerViewlayoutManager);
        // progressBar.setVisibility(View.VISIBLE);

        tipCursa = new ArrayList<>();
        codCursa = new ArrayList<>();
        statiePlecare = new ArrayList<>();
        statieSosire = new ArrayList<>();
        oraPlecare = new ArrayList<>();
        oraSosire = new ArrayList<>();
        dataPlecare = new ArrayList<>();

        /** Call data from json web. */
        search_direct_trips();

    }

    private void search_direct_trips() {

        String directTripsURL = "http://192.168.1.6/trenuri_cfr_tfc/search_trips.php?statie_plecare=" + st_plecare + "&statie_sosire=" + st_sosire + "&data_plecare=" + dt_plecare;
        directTripsURL = directTripsURL.replaceAll(" ", "%20");

        jsonArrayRequest = new JsonArrayRequest(directTripsURL,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        //progressBar.setVisibility(View.GONE);

                        get_direct_trips(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(jsonArrayRequest);

    }

    /**
     * Parse data from the website call.
     * @param array
     */
    public void get_direct_trips(JSONArray array){

        for(int i = 0; i<array.length(); i++) {

            Trains trainsAdapter = new Trains();

            JSONObject json;
            try {
                json = array.getJSONObject(i);

                trainsAdapter.setTipCursa(json.getString(tip_cursa));
                trainsAdapter.setCodCursa(json.getString(cod_cursa));
                trainsAdapter.setStatiePlecare(json.getString(statie_plecare));
                trainsAdapter.setStatieSosire(json.getString(statie_sosire));
                trainsAdapter.setOraPlecare(json.getString(ora_plecare));
                trainsAdapter.setOraSosire(json.getString(ora_sosire));
                trainsAdapter.setDataPlecare(dt_plecare);

                Log.d(TAG, "get_direct_trips: " + json.getString(tip_cursa) + json.getString(cod_cursa) + json.getString(statie_plecare) + json.getString(statie_sosire) + json.getString(ora_plecare) +
                        json.getString(ora_sosire) + dt_plecare);

                tipCursa.add(json.getString(tip_cursa));
                codCursa.add(json.getString(cod_cursa));
                statiePlecare.add(json.getString(statie_plecare));
                statieSosire.add(json.getString(statie_sosire));
                oraPlecare.add(json.getString(ora_plecare));
                Log.d(TAG, "get_direct_trips: " + oraPlecare);
                oraSosire.add(json.getString(ora_sosire));
                dataPlecare.add(dt_plecare);

            } catch (JSONException e) {

                e.printStackTrace();
            }

            trainList.add(trainsAdapter);
        }

        recyclerViewadapter = new RecyclerViewAdapter(trainList, this);

        recyclerView.setAdapter(recyclerViewadapter);

    }

}
