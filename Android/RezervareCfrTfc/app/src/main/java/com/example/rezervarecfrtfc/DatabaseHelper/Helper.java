package com.example.rezervarecfrtfc.DatabaseHelper;

import android.app.Activity;
import android.view.inputmethod.InputMethodManager;

public class Helper {

    // url de baza
    private static String MAIN_URL = "http://192.168.1.6/trenuri_cfr_tfc/";

//    // URL address for extra database
//    private static String EXTRA_URL = "http://192.168.1.6/server_trenuri";

    // login url
    public static String LOGIN_URL = MAIN_URL + "login.php";

    // register url
    public static String REGISTER_URL = MAIN_URL + "register.php";

    public static String CFR_ACTIVITY_URL = MAIN_URL + "cfrActivity.php";

    // URL address for searching station
    public final static String SEARCH_STATION = MAIN_URL + "search_station.php";

    /**
     *  Hide Keyboard
     */
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }
}
