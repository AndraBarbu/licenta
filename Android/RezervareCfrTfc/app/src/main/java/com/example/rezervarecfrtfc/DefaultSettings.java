package com.example.rezervarecfrtfc;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class DefaultSettings {
    private static SharedPreferences sharedPreferences;

    // create one method that will instantiate sharedPreferecdes
    private static void getSharedPreferencesInstance(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    // this is for switchPreference
    public static boolean notificationEnanbled(Context context) {
        getSharedPreferencesInstance(context);
        return sharedPreferences.getBoolean("notifications", true);
    }


    // editTextPreferece [password]
    public static String getUserPassword(Context context) {
        getSharedPreferencesInstance(context);
        return sharedPreferences.getString("password", "");
    }

    // editTextPreferece [password]
    public static String getUserEmail(Context context) {
        getSharedPreferencesInstance(context);
        return sharedPreferences.getString("email", "");
    }

    // editTextPreferece [password]
    public static String getUserNumber(Context context) {
        getSharedPreferencesInstance(context);
        return sharedPreferences.getString("phone", "");
    }
}