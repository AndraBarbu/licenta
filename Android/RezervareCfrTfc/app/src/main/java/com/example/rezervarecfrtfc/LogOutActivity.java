package com.example.rezervarecfrtfc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.rezervarecfrtfc.R;

public class LogOutActivity extends AppCompatActivity {
    private static final String TAG = "LogOutActivity";
    private static final int REQUEST_LOGIN = 0;
    public Button btnIesire;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        btnIesire.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the cfr activity
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivityForResult(intent, REQUEST_LOGIN);
            }
        });



    }
    }
