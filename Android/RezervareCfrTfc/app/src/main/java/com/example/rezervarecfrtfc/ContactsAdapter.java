package com.example.rezervarecfrtfc;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {

    private static final String TAG = RecyclerViewAdapter.class.getName();

    Context context;

    List<Contacts> contacts;

    public ContactsAdapter(List<Contacts> getDataAdapter, Context context){

        super();

        this.contacts = getDataAdapter;

        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_contact, parent, false);

        return new ViewHolder(view);
    }


    @SuppressLint("SetTextI18n")
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Contacts contactsAdapter =  contacts.get(position);

        holder.nume_operator.setText(contactsAdapter.getNumeOperator());
        holder.numar_operator.setText(contactsAdapter.getNumarTelefon());
        holder.email.setText(contactsAdapter.getEmail());
        holder.adresa.setText(contactsAdapter.getAdresa());
        holder.fax.setText(contactsAdapter.getFax());
        holder.operator_url.setText(contactsAdapter.getOperatorUrl());

    }

    @Override
    public int getItemCount() {

        return contacts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView nume_operator, numar_operator, email, adresa, fax;
        TextView operator_url;
        Context c;


        public ViewHolder(View itemView) {

            super(itemView);
            c = itemView.getContext();

            nume_operator = itemView.findViewById(R.id.nume_operator);
            numar_operator = itemView.findViewById(R.id.numar_telefon);
            email = itemView.findViewById(R.id.adresa_email);
            adresa = itemView.findViewById(R.id.adresa);
            fax = itemView.findViewById(R.id.fax);
            operator_url = itemView.findViewById(R.id.url);
        }
    }
}