package com.example.rezervarecfrtfc;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.rezervarecfrtfc.model.AutoCompleteAdapter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = SearchActivity.class.getName();

    private EditText data_plecare;
    private AutoCompleteTextView statie_de_plecare, statie_de_sosire;
    private AutoCompleteAdapter startAdapter, stopAdapter;
    private Button cautare;
    private CheckBox checkBox;
    private LinearLayout checkBox_layout;
    private int mYear, mMonth, mDay;
    private String str_statie_plecare, str_statie_sosire, str_data_plecare, str_nume_operator, raspuns;
    private boolean net = false;
    private List<CheckBox> checkBoxItems;

    private static final String OPERATOR_1 = "CFR Călători";
    private static final String OPERATOR_2 = "Transferoviar Calatori";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);

        statie_de_plecare = findViewById(R.id.oras_plecare);
        startAdapter = new AutoCompleteAdapter(this,android.R.layout.simple_dropdown_item_1line);
        statie_de_plecare.setAdapter(startAdapter);

        /** Set string name when autocomplete option is clicked. */
        statie_de_plecare.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (statie_de_plecare != null) {
                    String numeStatiePlecare = startAdapter.getItem(position).getName();
                    statie_de_plecare.setText(numeStatiePlecare);
                }
            }
        });

        /** Set string name when autocomplete option is clicked. */
        statie_de_sosire = findViewById(R.id.oras_sosire);
        stopAdapter = new AutoCompleteAdapter(this,android.R.layout.simple_dropdown_item_1line);
        statie_de_sosire.setAdapter(stopAdapter);

        // when autocomplete is clicked
        statie_de_sosire.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (statie_de_sosire != null) {
                    String numeStatieSosire = stopAdapter.getItem(position).getName();
                    statie_de_sosire.setText(numeStatieSosire);
                }
            }
        });

        data_plecare = findViewById(R.id.data_plecare);
        cautare = findViewById(R.id.cautare);
        checkBox = findViewById(R.id.checkBox);
        checkBox_layout = findViewById(R.id.checkBox_layout);

        cautare.setOnClickListener(this);
        data_plecare.setOnClickListener(this);
        checkBox_layout.setOnClickListener(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

        @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }


    @Override
    public void onClick(View v) {

        /** Check checkbox button if it's not checked. */
        if (v == checkBox_layout) {
            checkBox.setChecked(!checkBox.isChecked());
            //str_nume_operator = getTextCheckBox();
        }

        /** If the user selects data a calendar is displayed. */
        if (v == data_plecare) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            data_plecare.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();
        }

        /** It performs searches when button is clicked.
         * Look for conditions:
         * a) if no operator option is selected search for available operators otherwise search that one.
         * b) if checkbox is checked it searches just the direct routes otherwise search for all the trips. */
        if (v == cautare) {

            ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;

            if (activeNetwork != null) {
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                    raspuns = "You are connected to a WiFi Network";
                    net = true;
                }

                if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                    net = true;
                    raspuns = "You are connected to a Mobile Network";
                }

            } else
                raspuns = "No internet Connectivity";

            if (net) {

                str_statie_plecare = statie_de_plecare.getText().toString();
                str_statie_sosire = statie_de_sosire.getText().toString();
                str_data_plecare = data_plecare.getText().toString();

                Log.d(TAG, "onClick(): values - "
                        + " - " + str_statie_plecare
                        + " - " + str_statie_sosire
                        + " - " + str_data_plecare);

                /** If start and stop station values are not empty add it as extra to the intent. */
                if (!str_statie_plecare.isEmpty() && !str_statie_sosire.isEmpty()) {

                    Intent intent = new Intent(this, TrainsItineraries.class);
                    intent.putExtra("statie_plecare", str_statie_plecare);
                    intent.putExtra("statie_sosire", str_statie_sosire);

                    /** If the user doesn't select a start data then the current data wi'll be selected. */
                    if (str_data_plecare.isEmpty()) {

                        str_data_plecare = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
                    }

                    // add data extras to the intent
                    intent.putExtra("data_plecare", str_data_plecare);

//                    // add data extras to the intent
//                    if (!str_nume_operator.isEmpty()) {
//                        intent.putExtra("nume_operator", str_nume_operator);
//                    }

                    // start activity via the intent
                    startActivity(intent);

                } else if (str_statie_sosire.isEmpty())
                    Toast.makeText(SearchActivity.this, "Introduceți o stație de sosire", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(SearchActivity.this, "Introduceți o stație de plecare", Toast.LENGTH_SHORT).show();

            } else
                Toast.makeText(SearchActivity.this, raspuns, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.setari_cont:
                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                startActivity(settingsIntent);
                break;
            // action with ID action_settings was selected
            case R.id.iesire:
                Intent aboutIntent = new Intent(this, LoginActivity.class);
                startActivity(aboutIntent);
                break;
            case R.id.cum_fac_o_rezervare:
                Intent guideReservation = new Intent(this, ReservationsGuide.class);
                startActivity(guideReservation);
                break;
            case R.id.contact:
                Intent contact = new Intent(this, OperatorContacts.class);
                startActivity(contact);
                break;
            default:
                break;
        }

        return true;
    }

    private String getTextCheckBox() {
        String text = null;
        for (CheckBox item : checkBoxItems){
            if(item.isChecked())
                text = item.getText().toString();
        }
        return text;
    }
}