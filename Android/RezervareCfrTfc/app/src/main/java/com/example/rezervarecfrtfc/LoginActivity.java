package com.example.rezervarecfrtfc;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.view.WindowManager;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.example.rezervarecfrtfc.DatabaseHelper.Helper;
import com.example.rezervarecfrtfc.DatabaseHelper.ManagerSesiune;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    private static final int REQUEST_SIGNUP = 0;
    private static final int REQUEST_FORGET_PSWD = 0;

    @BindView(R.id.input_email) EditText _emailText;
    @BindView(R.id.input_password) EditText _passwordText;
    @BindView(R.id.btn_login) Button _loginButton;
    @BindView(R.id.link_signup) TextView _signupLink;
    @BindView(R.id.show_hide_password) TextView _showHidePwd;
    @BindView(R.id.forgot_password) TextView _forgotPwd;

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_PASSWORD = "password";
    private int success;

    private ManagerSesiune managerSesiune;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        PreferenceManager.setDefaultValues(this, R.xml.layout_settings, false);

        // session manager retine daca utilizatorul e logat sau nu
        managerSesiune = new ManagerSesiune(getApplicationContext());

        // check user is already logged in
        if (managerSesiune.isLoggedIn()) {
            Intent i = new Intent(LoginActivity.this, SearchActivity.class);
            startActivity(i);
            finish();
        }

        // Hide Keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //hide keyboard after press login btn
        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // hide keyboard when click on login button
                Helper.hideSoftKeyboard(LoginActivity.this);
                // call login method
                login();
            }
        });

        //signup link
        _signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Sign up activity
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });

        //forgot pswd btn
        _forgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Start Forgot pswd activity
                Intent intent = new Intent(getApplicationContext(), ForgetPasswordActivity.class);
                startActivityForResult(intent, REQUEST_FORGET_PSWD);
            }
        });

        // Set check listener over checkbox for showing and hiding password
        _showHidePwd.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                //show/hide pwd
                if(((CompoundButton) view).isChecked()){
                    _passwordText.setTransformationMethod(HideReturnsTransformationMethod
                            .getInstance());// show password
                } else {
                    _passwordText.setTransformationMethod(PasswordTransformationMethod
                            .getInstance());// hide password
                }

            }
        });

    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        _loginButton.setEnabled(false);

        /** Show progress bar */
        final ProgressDialog progressDialog = ProgressDialog.show(this, null, null, true, false);
        progressDialog.setContentView(R.layout.progress_layout);

        //get email and pass values
        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        //execute process login, send email and pass paramas for login
        new ProcessLogin().execute(email, password);

        //handler that execute action after make connection to bd
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        if (success == 1)
                            onLoginSuccess();
                        else
                            onLoginFailed();

                        // dismiss progress dialog
                        progressDialog.dismiss();
                    }
                }, 3000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                // By default we just finish the Activity and log them in automatically
                if (success == 1) {
                    // Finish the registration screen and return to the Login activity
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        // disable going back to the MainActivity
        moveTaskToBack(true);
    }


    public void onLoginSuccess() {

        // show success toast
        Toast.makeText(getBaseContext(), "Autentificare reusita", Toast.LENGTH_LONG).show();

        // login button enabled
        _loginButton.setEnabled(true);

        // change session to login state
        managerSesiune.isLoggedIn();
    }

    public void onLoginFailed() {
        // show error toast
        Toast.makeText(getBaseContext(), "Autentificare esuata", Toast.LENGTH_LONG).show();

        //  login button enabled
        _loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    /**
     * Clasa folosita pentru a procesa logarea utilizatorului.
     */
    @SuppressLint("StaticFieldLeak")
    class ProcessLogin extends AsyncTask<String, String, String> {
        //method from AsyncTask class
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                List<NameValuePair> params = new ArrayList<>();
                params.add(new BasicNameValuePair(TAG_EMAIL, strings[0]));
                params.add(new BasicNameValuePair(TAG_PASSWORD, strings[1]));

                Log.d("request!", "starting" + params);

                //make request http post - return json, connection with BD
                JSONObject json = JSONData.makeHttpRequest(Helper.LOGIN_URL, "POST",	params);

                Log.d("Login attempt", json.toString());

                success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    Log.d(TAG, json.toString() + " with success");
                    Intent i = new Intent(LoginActivity.this, SearchActivity.class);
                    startActivity(i);
                    return json.getString(TAG_MESSAGE);
                } else {
                    Log.d("Login Failure!", json.getString(TAG_MESSAGE));
                    return json.getString(TAG_MESSAGE);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String file_url) {
            if (file_url != null) {
                Toast.makeText(LoginActivity.this, file_url, Toast.LENGTH_LONG)
                        .show();
            }
        }

    }
}