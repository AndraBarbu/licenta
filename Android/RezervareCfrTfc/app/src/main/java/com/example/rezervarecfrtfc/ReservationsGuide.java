package com.example.rezervarecfrtfc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class ReservationsGuide  extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reservations_guide_layout);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_reservations, menu);
        return true;
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.setari_cont:
                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                startActivity(settingsIntent);
                break;
            // action with ID action_settings was selected
            case R.id.iesire:
                Intent aboutIntent = new Intent(this, LoginActivity.class);
                startActivity(aboutIntent);
                break;
            case R.id.rezervari:
                Intent guideReservation = new Intent(this, ReservationsGuide.class);
                startActivity(guideReservation);
                break;
            case R.id.contact:
                Intent contact = new Intent(this, Contacts.class);
                startActivity(contact);
                break;
            default:
                break;
        }

        return true;
    }

}

