package com.example.rezervarecfrtfc;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private static final String TAG = RecyclerViewAdapter.class.getName();

    Context context;

    List<Trains> trains;

    public RecyclerViewAdapter(List<Trains> getDataAdapter, Context context){

        super();

        this.trains = getDataAdapter;

        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_trips, parent, false);

        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Trains trainsAdapter =  trains.get(position);

        holder.trip_1.setText(trainsAdapter.getTipCursa() + " " + trainsAdapter.getCodCursa());
        holder.statie_plecare.setText(trainsAdapter.getStatiePlecare());
        holder.ora_plecare.setText(trainsAdapter.getOraPlecare());
        holder.statie_sosire.setText(trainsAdapter.getStatieSosire());
        holder.ora_sosire.setText(trainsAdapter.getOraSosire());
        //holder.data_plecare.setText(trainsAdapter.getDataPlecare());

    }

    @Override
    public int getItemCount() {

        return trains.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView statie_plecare, statie_sosire, ora_plecare, ora_sosire, data_plecare;
        TextView trip_1;
        //Button price_sl, price_1, price_2;
        Context c;


        public ViewHolder(View itemView) {

            super(itemView);
            c = itemView.getContext();
            Log.d(TAG, "ViewHolder: " + data_plecare);

            trip_1 = itemView.findViewById(R.id.id_train_1);
            statie_plecare = itemView.findViewById(R.id.statie_plecare);
            statie_sosire = itemView.findViewById(R.id.statie_sosire);
            ora_plecare = itemView.findViewById(R.id.ora_plecare);
            ora_sosire = itemView.findViewById(R.id.ora_sosire);
            data_plecare = itemView.findViewById(R.id.data_plecare);
        }
    }
}