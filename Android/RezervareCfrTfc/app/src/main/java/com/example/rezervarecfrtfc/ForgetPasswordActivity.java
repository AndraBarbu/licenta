package com.example.rezervarecfrtfc;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rezervarecfrtfc.DatabaseHelper.Helper;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ForgetPasswordActivity extends AppCompatActivity {
    private static final String TAG = "ForgetPasswordActivity";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_PASSWORD = "password";
    private static final String TAG_password = "password";
    private static final String TAG_MESSAGE = "message";
    private String password;


    @BindView(R.id.registered_email) EditText _emailRegistered;
    @BindView(R.id.backToLoginBtn) TextView _back;
    @BindView(R.id.forgot_button) TextView _submit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);
        ButterKnife.bind(this);

        _back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });

        _submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new ProcessLogin().execute(_emailRegistered.getText().toString());
                sendEmail();
                // show password toast
                Toast.makeText(getBaseContext(), "Verifica email", Toast.LENGTH_LONG).show();


                try { 
                    Thread.sleep(5000); 
                }
                catch (InterruptedException ex) { 
                    android.util.Log.d(TAG, ex.toString()); }

                // Finish the registration screen and return to the Login activity
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
    }

    public void sendEmail(){
        String subject = "Parola Star Travel";
        String emailbody = "Parola contului dumneavoastra este: " ;

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:" + _emailRegistered));
        emailIntent.setType("plain/text");
        startActivity(Intent.createChooser(emailIntent, "Send feedback"));
        //emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{_emailRegistered.getText().toString()});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, emailbody);
        ForgetPasswordActivity.this.startActivity(Intent.createChooser(emailIntent, "Trimite email..."));

    }

    @SuppressLint("StaticFieldLeak")
    class ProcessLogin extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                List<NameValuePair> params = new ArrayList<>();
                params.add(new BasicNameValuePair(TAG_PASSWORD, strings[0]));

                Log.d("request!", "starting" + params);

                JSONObject json = JSONData.makeHttpRequest(Helper.LOGIN_URL, "POST",	params);

                Log.d("Login attempt", json.toString());

                password = json.getString(TAG_PASSWORD);

                if (password.length() >= 1) {
                    Log.d(TAG, json.toString() + " with password");
                    Intent i = new Intent(ForgetPasswordActivity.this, LoginActivity.class);
                    startActivity(i);
                    return json.getString(TAG_PASSWORD);
                } else {
                    Log.d("Login Failure!", json.getString(TAG_PASSWORD));
                    return json.getString(TAG_PASSWORD);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String file_url) {
            if (file_url != null) {
                Toast.makeText(ForgetPasswordActivity.this, file_url, Toast.LENGTH_LONG)
                        .show();
            }
        }

    }
}