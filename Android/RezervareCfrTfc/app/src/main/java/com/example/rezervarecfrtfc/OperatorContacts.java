package com.example.rezervarecfrtfc;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OperatorContacts extends AppCompatActivity {

    public static final String TAG = TrainsItineraries.class.getName();

    List<Contacts> contactsList;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager recyclerViewlayoutManager;
    RecyclerView.Adapter recyclerViewadapter;

    String nume_op, nr_op, email_op, adresa_op, fax_op, url_op;

    String nume_operator = "nume_operator";
    String numar_telefon = "numar_telefon";
    String email = "email";
    String adresa = "adresa";
    String fax = "fax";
    String operator_url = "operator_url";

    JsonArrayRequest jsonArrayRequest;

    RequestQueue requestQueue;

    /** list of arrays with all the information about trips. */
    ArrayList<String> numeOperator, numarOperator, Email, Adresa, Fax, operatorUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_layout);
        Intent intent=this.getIntent();
        assert intent != null;

        // get extras from intent
        nume_op = intent.getStringExtra("nume_operator");
        url_op = intent.getStringExtra("operator_url");
        nr_op = intent.getStringExtra("numar_telefon");
        email_op= intent.getStringExtra("email");
        adresa_op = intent.getStringExtra("adresa");
        fax_op = intent.getStringExtra("fax");


        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(OperatorContacts.this);
        @SuppressLint("CommitPrefEdits")
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("nume_operator", nume_op);

        /** create an array list of trip trains. */
        contactsList = new ArrayList<>();

        recyclerView =  findViewById(R.id.recyclerView2);
        recyclerView.setHasFixedSize(false);
        recyclerViewlayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(recyclerViewlayoutManager);

        numeOperator = new ArrayList<>();
        numarOperator = new ArrayList<>();
        Email = new ArrayList<>();
        Adresa = new ArrayList<>();
        Fax = new ArrayList<>();
        operatorUrl = new ArrayList<>();


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /** Call data from json web. */
        show_contact_info();

    }

    private void show_contact_info() {

        String contactsURL = "http://192.168.1.6/trenuri_cfr_tfc/contact.php";

        jsonArrayRequest = new JsonArrayRequest(contactsURL,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        //progressBar.setVisibility(View.GONE);

                        get_contacts_respons(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(jsonArrayRequest);

    }

    /**
     * Parse data from the website call.
     * @param array
     */
    public void get_contacts_respons(JSONArray array){

        for(int i = 0; i<array.length(); i++) {

            Contacts contactsAdapter = new Contacts();

            JSONObject json;
            try {
                json = array.getJSONObject(i);

                contactsAdapter.setNumeOperator(json.getString(nume_operator));
                contactsAdapter.setOperatorUrl(json.getString(operator_url));
                contactsAdapter.setNumarTelefon(json.getString(numar_telefon));
                contactsAdapter.setEmail(json.getString(email));
                contactsAdapter.setAdresa(json.getString(adresa));
                contactsAdapter.setFax(json.getString(fax));

                Log.d(TAG, "get_direct_trips: " + json.getString(nume_operator) + json.getString(operator_url) + json.getString(numar_telefon) + json.getString(email) + json.getString(adresa) +
                        json.getString(fax));

                numeOperator.add(json.getString(nume_operator));
                operatorUrl.add(json.getString(operator_url));
                numarOperator.add(json.getString(numar_telefon));
                Email.add(json.getString(email));
                Adresa.add(json.getString(adresa));
                Fax.add(json.getString(fax));

            } catch (JSONException e) {

                e.printStackTrace();
            }

            contactsList.add(contactsAdapter);
        }

        recyclerViewadapter = new ContactsAdapter(contactsList, this);

        recyclerView.setAdapter(recyclerViewadapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_contacts, menu);
        return true;
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.setari_cont:
                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                startActivity(settingsIntent);
                break;
            // action with ID action_settings was selected
            case R.id.iesire:
                Intent aboutIntent = new Intent(this, LoginActivity.class);
                startActivity(aboutIntent);
                break;
            case R.id.cum_fac_o_rezervare:
                Intent guideReservation = new Intent(this, ReservationsGuide.class);
                startActivity(guideReservation);
                break;
//            case R.id.rezervari:
//                Intent rezervare = new Intent(this, ReservationsGuide.class);
//                startActivity(rezervare);
//                break;
            default:
                break;
        }

        return true;
    }

}
