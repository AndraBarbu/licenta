package com.example.rezervarecfrtfc.model;

public class Station {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
