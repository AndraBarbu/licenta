package com.example.rezervarecfrtfc;

public class Trains {

    String cod_cursa, tip_cursa, statie_plecare, ora_plecare, statie_sosire, ora_sosire, data_plecare;

    public String getCodCursa()
    {
        return cod_cursa;
    }

    public void setCodCursa(String temp)
    {
        this.cod_cursa = temp;
    }

    public String getTipCursa()
    {
        return tip_cursa;
    }

    public void setTipCursa(String temp)
    {
        this.tip_cursa = temp;
    }

    public String  getStatiePlecare()
    {
        return statie_plecare;
    }

    public void setStatiePlecare(String temp)
    {
        this.statie_plecare = temp;
    }

    public String getOraPlecare()
    {
        return ora_plecare;
    }

    public void setOraPlecare(String temp)
    {
        this.ora_plecare = temp;
    }

    public String  getStatieSosire()
    {
        return statie_sosire;
    }

    public void setStatieSosire(String temp)
    {
        this.statie_sosire = temp;
    }

    public String  getOraSosire()
    {
        return ora_sosire;
    }

    public void setOraSosire(String temp)
    {
        this.ora_sosire = temp;
    }

    public String  getDataPlecare()
    {
        return data_plecare;
    }

    public void setDataPlecare(String temp)
    {
        this.data_plecare = temp;
    }
}