<?php

require('config.php');

if(!empty($_POST)){

    // email , password
    if(empty($_POST['email'])||empty($_POST['password'])){

        $raspuns["success"] = 0;

        $raspuns["mesaj"] = "Toate campurile sunt necesare";

        die(json_encode($raspuns));
    }

    $query = "
            SELECT `email`, `password` FROM `utilizator` 
            WHERE
            email = :email
             ";

    $query_params = array(
        ':email' => $_POST['email']
    );

    try {

        $stmt = $db->prepare($query);

        $result = $stmt->execute($query_params);
    } catch (PDOException $ex) {

        $raspuns['success'] = 0 ;
        $raspuns['mesaj'] = "Eroare la accesarea bazei de date";
        die(json_encode($raspuns));
    }

    $is_login = false;

    $encr_user_pass = $_POST['password'];

    $row = $stmt->fetch();

    if ($row){
        if($encr_user_pass === $row['password']){

            $is_login = true;
        }
    }

    // verifica daca login-ul este reusit
    if($is_login){

        // {"success":1,"mesaj":"Login reusit"}

        $raspuns["success"] = 1;
        $raspuns["email"] = $row["email"];
        $raspuns["mesaj"] = "Login reusit";

        die(json_encode($raspuns));

    } else {

        // {"success":0,"mesaj":"email sau parola incorecte"}

        $raspuns["success"] = 0;
        $raspuns["mesaj"] = "email sau parola incorecte";
        die(json_encode($raspuns));
    }

}else{
    ?>
    <h1>Login</h1>
    <form action="login.php" method="post">
        Email: <br/>
        <input type="text" name="email" placeholder="Email"/><br/>
        Password:<br/>
        <input type="password" name="password" placeholder="Password"/><br/>
        <input type="submit" value="Login"/>
        <a href="test.php">Test</a>
    </form>
    <?php
}
?>