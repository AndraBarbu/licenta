<?php

require ("config.php");

    $query = "select nume_operator, operator_url, numar_telefon, email, adresa, fax from trenuri.operator";

    try {

        $stmt = $db->prepare($query);
        $stmt->execute();

        if($stmt->rowCount())
        {
            $row_all = $stmt->fetchall(PDO::FETCH_ASSOC);
            header('Content-type: application/json');

            echo json_encode($row_all);

        }

    } catch (PDOException $exception) {

        $raspuns["success"] = 0;
        $raspuns["mesaj"] = "Ceva nu a functionat.Va rugam sa incercati mai tarziu";
        die(json_encode($raspuns));
    }
