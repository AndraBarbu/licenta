<?php

require("config.php");

if(!empty($_POST)){

    if(empty($_POST['username']) || empty($_POST['password']) || empty($_POST['email']) || empty($_POST['nr_tel'])){

        $raspuns["success"] = 0;

        $raspuns["mesaj"] = "All Fields Required";

        die(json_encode($raspuns));
    }

    $query = "SELECT COUNT(*) AS count 
				   FROM utilizator 
				   WHERE 
				   email = :email";

    $query_params = array(
        ':email' => $_POST['email']
    );

    try{
        $stmt = $db->prepare($query);
        $result = $stmt->execute($query_params);

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $email_count = $row["count"];
        }
        if ($email_count > 0) {

            $raspuns["success"] = 0;
            $raspuns["mesaj"] = "That email is already taken. Please try again.";

            die(json_encode($raspuns));

        }

    }catch(PDOException $ex){

        $raspuns["success"] = 0;
        $raspuns["mesaj"] = "Something went wrong. Please try again later";
        die(json_encode($raspuns));

    }

    $query = "INSERT INTO 
                        utilizator (`cod_u`, `username`, `password`, `email`, `nr_tel`) 
			            VALUES 
				        (:cod_u, :username, :password, :email, :nr_tel)";

    $digits = 3;

    $query_params = array(
        ':cod_u' => rand(pow(10, $digits-1), pow(10, $digits)-1),
        ':username' => $_POST['username'],
        ':password' =>  $_POST['password'],
        ':email' => $_POST['email'],
        ':nr_tel' => $_POST['nr_tel']
    );

    try {
        $stmt = $db->prepare($query);
        $result = $stmt->execute($query_params);

    } catch (PDOException $ex) {

        $raspuns["success"] = 0;
        $raspuns["mesaj"] = "The username is already in use, please try again later!";

        die(json_encode($raspuns));
    }

    $raspuns["success"] = 1;
    $raspuns["mesaj"] = "Username Successfully Added";

    echo json_encode($raspuns);

}else{
    ?>

    <h1>Inregistrare</h1>
    <form action="register.php" method="post">
        Nume: <br/>
        <input type="text" name="username" placeholder="Nume"/><br/>
        Password: <br/>
        <input name="password" type="password" placeholder="Password"/><br/>
        Email: <br/>
        <input type="text" name="email" placeholder="Email"/><br/>
        Telefon: <br/>
        <input type="text" name="nr_tel" placeholder="Telefon"/><br/>

        <input type="submit" value="Register User"/>
    </form>
    <?php
}
?>