<?php

require ("config.php");

$def_op_array = array("CFR Călători", "Transferoviar Calatori");

if(!empty($_GET)) {

    if (empty($_GET["statie_plecare"]) || (empty($_GET["statie_sosire"]))) {

        $raspuns["success"] = 0;

        $raspuns["mesaj"] = "All fields required";

        die(json_encode($raspuns));
    }

    $data_plecare = $_GET['data_plecare'];

    /**
     *  Show day from date as string.
     * @param $date
     * @return false|string
     */
    function dayName($date) {

        $convert_date = strtotime($date);
        $name_day = date('l',$convert_date);

        $result = $name_day;

        return $result;
    }

    $day_untranslated = dayName($data_plecare);

    /**
     * Translate name of date and assign to the @day var.
     * @param $untranslated_day
     * @return string
     */
    function translate_day($untranslated_day)
    {
        switch ($untranslated_day) {

            case "Sunday":
                $translated_date = "duminica";
                break;

            case "Monday":
                $translated_date = "luni";
                break;

            case "Tuesday":
                $translated_date = "marti";
                break;

            case "Wednesday":
                $translated_date = "miercuri";
                break;

            case "Thursday":
                $translated_date = "joi";
                break;

            case "Friday":
                $translated_date = "vineri";
                break;

            case "Saturday":
                $translated_date = "sambata";
                break;
        }
        return $translated_date;
    }

    $day = translate_day($day_untranslated);

    /**
     * If operator name exists then set as array to the operator name
     * otherwise set default values for the operator names.
     */
    if(isset($_GET['nume_operator'])) {
        /* operator name exists */
        $nume_operator = $_GET['nume_operator'];
        $def_op_array = array($nume_operator, null);
    }

    $query = "SELECT ce.denumire_ce as tip_cursa, 
                    ce.cod_ce AS cod_cursa,
                    start_statie.nume_statie as statie_plecare,
		            start_st.ora_plecare as ora_plecare,
                    stop_statie.nume_statie as statie_sosire,
                    stop_st.ora_sosire as ora_sosire
			FROM cursa_efectiva ce
			        INNER JOIN calendar cld ON ce.service_id = cld.service_id
                    INNER JOIN cursa crs ON ce.cod_cursa = crs.cod_cursa
                    INNER JOIN operator op ON crs.cod_operator = op.cod_operator
                    INNER JOIN cursa_efectiva_statie start_st ON ce.cod_ce = start_st.cod_ce
                    INNER JOIN statie start_statie ON start_st.cod_s = start_statie.cod_s
                    INNER JOIN cursa_efectiva_statie stop_st ON ce.cod_ce = stop_st.cod_ce
                    INNER JOIN statie stop_statie ON stop_st.cod_s = stop_statie.cod_s
            WHERE start_st.ora_plecare > '00:00:00' AND start_st.ora_plecare < '23:59:59' AND 
				    start_statie.nume_statie = :statie_plecare AND
				    stop_st.ora_sosire <> '' AND 
				    stop_statie.nume_statie = :statie_sosire AND 
				    cld.$day = 1
				    AND op.nume_operator IN ('".implode("','",$def_op_array)."')
				    ";

    $query_params = array(
        ":statie_plecare" => $_GET["statie_plecare"],
        ":statie_sosire" => $_GET["statie_sosire"]
    );

    try {

        $stmt = $db->prepare($query);
        $stmt->execute($query_params);

        if($stmt->rowCount())
        {
            $row_all = $stmt->fetchall(PDO::FETCH_ASSOC);
            header('Content-type: application/json');

            echo json_encode($row_all);

        }

    } catch (PDOException $exception) {

        $raspuns["success"] = 0;
        $raspuns["mesaj"] = "Ceva nu a functionat.Va rugam sa incercati mai tarziu";
        die(json_encode($raspuns));
    }

} else {

    ?>

    <h1>Cautare ruta</h1>
    <form action="search_trips.php" method="get">
        Plecare de la: <br/>
        <input type="text" name="statie_plecare" placeholder="Statie plecare"/><br/>
        Sosire la: <br/>
        <input type="text" name="statie_sosire" placeholder="Statie sosire"/><br/>
        Data plecare: <br/>
        <input type="date" name="data_plecare" placeholder="Data plecare"/><br/>

        <input type="submit" value="Cauta cursa"/>
    </form>
    <?php
}

?>
