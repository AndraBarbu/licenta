<?php

require ("config.php");

if(!empty($_GET)) {

    if (empty($_GET["nume_statie"])) {

        $raspuns["success"] = 0;
        $raspuns["mesaj"] = "All fields required";

        die(json_encode($raspuns));
    }

    $nume_statie = $_GET['nume_statie'];

    $query = "SELECT nume_statie FROM `statie` WHERE `nume_statie` LIKE '%{$nume_statie}%'";;
    $statement = $db->prepare($query);
    $statement->execute();

    if ($statement->rowCount()) {
        $row_all = $statement->fetchall(PDO::FETCH_ASSOC);
        header('Content-type: application/json');
        echo json_encode($row_all);

    } else if (!$statement->rowCount()) {
        // {"success":0,"mesaj":"email sau parola incorecte"}

        $raspuns["success"] = 0;
        $raspuns["mesaj"] = "date incorecte";
        die(json_encode($raspuns));
    }
}
